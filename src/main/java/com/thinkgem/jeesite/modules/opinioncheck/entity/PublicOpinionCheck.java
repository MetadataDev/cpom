/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinioncheck.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 舆情办理审核Entity
 * @author dingshuang
 * @version 2016-06-24
 */
public class PublicOpinionCheck extends DataEntity<PublicOpinionCheck> {
	
	private static final long serialVersionUID = 1L;
	private String opinionId;		// opinion_id
	private String submitDeptCode;		// 提交单位代码
	private String submitDeptName;		// 提交单位
	private String result;		// 审核结果
	private String reason;		// 原因（未通过）
	private Date checkDate;		// 审核时间
	
	public PublicOpinionCheck() {
		super();
	}

	public PublicOpinionCheck(String id){
		super(id);
	}

	@Length(min=0, max=64, message="opinion_id长度必须介于 0 和 64 之间")
	public String getOpinionId() {
		return opinionId;
	}

	public void setOpinionId(String opinionId) {
		this.opinionId = opinionId;
	}
	
	@Length(min=0, max=64, message="提交单位代码长度必须介于 0 和 64 之间")
	public String getSubmitDeptCode() {
		return submitDeptCode;
	}

	public void setSubmitDeptCode(String submitDeptCode) {
		this.submitDeptCode = submitDeptCode;
	}
	
	@Length(min=0, max=200, message="提交单位长度必须介于 0 和 200 之间")
	public String getSubmitDeptName() {
		return submitDeptName;
	}

	public void setSubmitDeptName(String submitDeptName) {
		this.submitDeptName = submitDeptName;
	}
	
	@Length(min=0, max=2, message="审核结果长度必须介于 0 和 2 之间")
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}
	
}