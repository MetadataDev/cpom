/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinioncheck.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.opinioncheck.entity.PublicOpinionCheck;
import com.thinkgem.jeesite.modules.publicoption.entity.PublicOpinion;

import java.util.List;

/**
 * 舆情办理审核DAO接口
 * @author dingshuang
 * @version 2016-06-24
 */
@MyBatisDao
public interface PublicOpinionCheckDao extends CrudDao<PublicOpinionCheck> {

    List<PublicOpinionCheck>  getListByPublicOpinion(PublicOpinion publicOpinion);

}