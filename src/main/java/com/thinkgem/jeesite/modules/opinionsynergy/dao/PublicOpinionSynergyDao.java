/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionsynergy.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.opinionsynergy.entity.PublicOpinionSynergy;
import com.thinkgem.jeesite.modules.publicoption.entity.PublicOpinion;

import java.util.List;

/**
 * 舆情协办信息管理DAO接口
 * @author dingshuang
 * @version 2016-06-21
 */
@MyBatisDao
public interface PublicOpinionSynergyDao extends CrudDao<PublicOpinionSynergy> {

    List<PublicOpinionSynergy> getListByPublicOpinion(PublicOpinion publicOpinion);
	
}