/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionmonitoring.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.opinionmonitoring.entity.OpinionMonitoring;

import java.util.List;

/**
 * 监控信息管理DAO接口
 * @author dingshuang
 * @version 2016-07-11
 */
@MyBatisDao
public interface OpinionMonitoringDao extends CrudDao<OpinionMonitoring> {

    List<OpinionMonitoring> findDeleteMonitoringOpinionList(OpinionMonitoring entity);

    int reverseDelete(OpinionMonitoring entity);

    List<OpinionMonitoring> findGeneralPage (OpinionMonitoring opinionMonitoring);

    void updateUpgradeMonitoring(String id);

    void updateDowngradeMonitoring(String id);

    List<OpinionMonitoring> findOpinionMonitoringRecovery(OpinionMonitoring opinionMonitoring);

    List<OpinionMonitoring> findGeneralMonitoringRecovery(OpinionMonitoring opinionMonitoring);
}