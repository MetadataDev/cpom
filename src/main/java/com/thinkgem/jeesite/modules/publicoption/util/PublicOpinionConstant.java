/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.publicoption.util;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.modules.opinionsynergy.entity.PublicOpinionSynergy;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import org.hibernate.validator.constraints.Length;

import java.util.Date;
import java.util.List;

/**
 * 舆情监控常量
 * @author dingshuang
 * @version 2016-06-21
 */
public class PublicOpinionConstant  {

	/**
	 * 信息中心账户（舆情信息分发者）
	 */
	public static  String  USER_XXZX_ACCOUNT ="xxzx";


}