/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.netpolitics.web;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.netpolitics.entity.PoliticalMail;
import com.thinkgem.jeesite.modules.netpolitics.service.PoliticalMailService;
import com.thinkgem.jeesite.modules.netpolitics.util.EntityUtil;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.service.OfficeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 网络问政Controller
 * @author dingshuang
 * @version 2016-06-15
 */
@Controller
@RequestMapping(value = "${adminPath}/netpolitics/politicalMail")
public class PoliticalMailController extends BaseController {

	@Autowired
	private PoliticalMailService politicalMailService;

	@Autowired
	private OfficeService officeService;

	
	@ModelAttribute
	public PoliticalMail get(@RequestParam(required=false) String id) {
		PoliticalMail entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = politicalMailService.get(id);
		}
		if (entity == null){
			entity = new PoliticalMail();
		}
		return entity;
	}
	
	@RequiresPermissions("netpolitics:politicalMail:view")
	@RequestMapping(value = {"list", ""})
	public String list(PoliticalMail politicalMail, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<PoliticalMail> page = politicalMailService.findPage(new Page<PoliticalMail>(request, response), politicalMail); 
		model.addAttribute("page", page);
		return "modules/netpolitics/politicalMailList";
	}



	@RequiresPermissions("netpolitics:politicalMail:edit")
	@RequestMapping(value = "save")
	public String save(PoliticalMail politicalMail, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, politicalMail)){
			return form(politicalMail, model);
		}
		politicalMailService.save(politicalMail);
		addMessage(redirectAttributes, "保存网络问政成功");
		return "redirect:"+Global.getAdminPath()+"/netpolitics/politicalMail/?repage";
	}




	
	@RequiresPermissions("netpolitics:politicalMail:edit")
	@RequestMapping(value = "delete")
	public String delete(PoliticalMail politicalMail, RedirectAttributes redirectAttributes) {
		politicalMailService.delete(politicalMail);
		addMessage(redirectAttributes, "删除网络问政成功");
		return "redirect:"+Global.getAdminPath()+"/netpolitics/politicalMail/?repage";
	}



	@RequiresPermissions("netpolitics:politicalMail:view")
	@RequestMapping(value = "form")
	public String form(PoliticalMail politicalMail, Model model) {
		model.addAttribute("politicalMail", politicalMail);
		String taskDefKey = politicalMail.getAct().getTaskDefKey();
		model.addAttribute("task",taskDefKey);
		return "modules/netpolitics/politicalMailForm";
	}


	/**
	 * 工单执行（完成任务）
	 * @param politicalMail
	 * @param model
	 * @return
	 */
	@RequiresPermissions("netpolitics:politicalMail:edit")
	@RequestMapping(value = "mailsave")
	public String saveAudit(PoliticalMail politicalMail, Model model,HttpServletRequest request) {
		politicalMailService.mailsave(politicalMail,request);
		return "redirect:" + adminPath + "/act/task/todo/";
	}



	/**
	 * 该方法支持异步jsonp跨域获取所有相关的信息
	 *
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "getMsg")
	public void getMsg(HttpServletRequest request, HttpServletResponse response,PoliticalMail politicalMail) throws IOException {
		PrintWriter out = response.getWriter();
		String callbackName = request.getParameter("callbackParam");
		String function = request.getParameter("function");
		Object result = null; // 返回数据结果
		if ("queryMailList".equals(function)) {
			// 要求查询的是信件信息列表
			Page<PoliticalMail> page = politicalMailService.findPage(new Page<PoliticalMail>(request, response), politicalMail);
			//为了避免json过深的情况，获取主要属性
			result = EntityUtil.getMainProperty(page.getList(),new String[]{"id"});
		}else if ("getOrgList".equals(function)) {
			// 该方法用于获取机构列表
			List<Map<String,Object>> leafOffices = new ArrayList<Map<String,Object>>();
			result = officeService.getAllLeafOffice(officeService.get(new Office("1")),leafOffices);
		}else if ("addMail".equals(function)) {
			// 该方法用于添加信件
			result = politicalMailService.sendMail(politicalMail);
		}else if ("queryMailDetail".equals(function)) {
			// 要求查询的是信件的详细信息
			result = EntityUtil.getDeclaredFieldsToMap(politicalMailService.get(politicalMail),null);
		}
		out.write(callbackName + "(" + JSONObject.valueToString(result).toString() + ")");
	}

}