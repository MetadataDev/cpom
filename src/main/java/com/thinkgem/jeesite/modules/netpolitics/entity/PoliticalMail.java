/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.netpolitics.entity;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 网络问政Entity
 * @author dingshuang
 * @version 2016-06-15
 */
public class PoliticalMail extends ActEntity<PoliticalMail> {
	
	private static final long serialVersionUID = 1L;
	private String queryCode;		// 查询代码
	private String queryPassword;		// 查询密码
	private String subject;		// 来信主题
	private String name;		// 姓名
	private String mobile;		// 联系电话
	private String pcount;		// 来访人数
	private String addr;		// 家庭住址
	private String mail;		// 电子邮件
	private String aproval;		// 来访目的
	private String mailType;		// 信件类型
	private String classify;		// 问题分类
	private String mcontent;		// 信件内容
	private String attach;		// 附件
	private String open;		// 是否公开
	private String state;		// 是否处理
	private Date applyTiime;		// 提交时间
	private String hdept;		// 受理部门
	private Date handleTime;		// 办理时间
	private String handler;		// 受理人
	private String handlerContent;		// 回复内容
	private String processInstanceId;		// 流程id
	private Date beginApplyTiime;		// 开始 提交时间
	private Date endApplyTiime;		// 结束 提交时间
	private Date beginHandleTime;		// 开始 办理时间
	private Date endHandleTime;		// 结束 办理时间



	private String warningState = "1";//预警状态
	
	public PoliticalMail() {
		super();
	}

	public PoliticalMail(String id){
		super(id);
	}

	@Length(min=0, max=36, message="查询代码长度必须介于 0 和 36 之间")
	public String getQueryCode() {
		return queryCode;
	}

	public void setQueryCode(String queryCode) {
		this.queryCode = queryCode;
	}
	
	@Length(min=0, max=36, message="查询密码长度必须介于 0 和 36 之间")
	public String getQueryPassword() {
		return queryPassword;
	}

	public void setQueryPassword(String queryPassword) {
		this.queryPassword = queryPassword;
	}
	
	@Length(min=1, max=500, message="来信主题长度必须介于 1 和 500 之间")
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	@Length(min=1, max=100, message="姓名长度必须介于 1 和 100 之间")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=1, max=15, message="联系电话长度必须介于 1 和 15 之间")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	@Length(min=0, max=10, message="来访人数长度必须介于 0 和 10 之间")
	public String getPcount() {
		return pcount;
	}

	public void setPcount(String pcount) {
		this.pcount = pcount;
	}
	
	@Length(min=0, max=100, message="家庭住址长度必须介于 0 和 100 之间")
	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}
	
	@Length(min=1, max=50, message="电子邮件长度必须介于 1 和 50 之间")
	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}
	
	@Length(min=1, max=10, message="来访目的长度必须介于 1 和 10 之间")
	public String getAproval() {
		return aproval;
	}

	public void setAproval(String aproval) {
		this.aproval = aproval;
	}
	
	@Length(min=0, max=10, message="信件类型长度必须介于 0 和 10 之间")
	public String getMailType() {
		return mailType;
	}

	public void setMailType(String mailType) {
		this.mailType = mailType;
	}
	
	@Length(min=1, max=10, message="问题分类长度必须介于 1 和 10 之间")
	public String getClassify() {
		return classify;
	}

	public void setClassify(String classify) {
		this.classify = classify;
	}
	
	public String getMcontent() {
		return mcontent;
	}

	public void setMcontent(String mcontent) {
		this.mcontent = mcontent;
	}
	
	@Length(min=0, max=100, message="附件长度必须介于 0 和 100 之间")
	public String getAttach() {
		return attach;
	}

	public void setAttach(String attach) {
		this.attach = attach;
	}
	
	@Length(min=1, max=10, message="是否公开长度必须介于 1 和 10 之间")
	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}
	
	@Length(min=0, max=10, message="是否处理长度必须介于 0 和 10 之间")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getApplyTiime() {
		return applyTiime;
	}

	public void setApplyTiime(Date applyTiime) {
		this.applyTiime = applyTiime;
	}
	
	@Length(min=0, max=36, message="受理部门长度必须介于 0 和 36 之间")
	public String getHdept() {
		return hdept;
	}

	public void setHdept(String hdept) {
		this.hdept = hdept;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getHandleTime() {
		return handleTime;
	}

	public void setHandleTime(Date handleTime) {
		this.handleTime = handleTime;
	}
	
	@Length(min=0, max=36, message="受理人长度必须介于 0 和 36 之间")
	public String getHandler() {
		return handler;
	}

	public void setHandler(String handler) {
		this.handler = handler;
	}
	
	public String getHandlerContent() {
		return handlerContent;
	}

	public void setHandlerContent(String handlerContent) {
		this.handlerContent = handlerContent;
	}
	
	@Length(min=0, max=36, message="流程id长度必须介于 0 和 36 之间")
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	
	public Date getBeginApplyTiime() {
		return beginApplyTiime;
	}

	public void setBeginApplyTiime(Date beginApplyTiime) {
		this.beginApplyTiime = beginApplyTiime;
	}
	
	public Date getEndApplyTiime() {
		return endApplyTiime;
	}

	public void setEndApplyTiime(Date endApplyTiime) {
		this.endApplyTiime = endApplyTiime;
	}
		
	public Date getBeginHandleTime() {
		return beginHandleTime;
	}

	public void setBeginHandleTime(Date beginHandleTime) {
		this.beginHandleTime = beginHandleTime;
	}
	
	public Date getEndHandleTime() {
		return endHandleTime;
	}

	public void setEndHandleTime(Date endHandleTime) {
		this.endHandleTime = endHandleTime;
	}


	public String getWarningState() {
		return warningState;
	}

	public void setWarningState(String warningState) {
		this.warningState = warningState;
	}
}