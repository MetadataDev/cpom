/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.netpolitics.service;

import java.text.SimpleDateFormat;
import java.util.*;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.emailconfig.entity.EmailConfiger;
import com.thinkgem.jeesite.modules.emailconfig.service.EmailConfigerService;
import com.thinkgem.jeesite.modules.netpolitics.util.PoliticalMailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.netpolitics.entity.PoliticalMail;
import com.thinkgem.jeesite.modules.netpolitics.dao.PoliticalMailDao;

import javax.servlet.http.HttpServletRequest;

/**
 * 网络问政Service
 *
 * @author dingshuang
 * @version 2016-06-15
 */
@Service
@Transactional(readOnly = true)
public class PoliticalMailService extends CrudService<PoliticalMailDao, PoliticalMail> {

    @Autowired
    private ActTaskService actTaskService;

    @Autowired
    private EmailConfigerService emailConfigerService;


    public PoliticalMail get(String id) {
        return super.get(id);
    }

    public List<PoliticalMail> findList(PoliticalMail politicalMail) {
        return super.findList(politicalMail);
    }

    public Page<PoliticalMail> findPage(Page<PoliticalMail> page, PoliticalMail politicalMail) {
        return super.findPage(page, politicalMail);
    }

    @Transactional(readOnly = false)
    public void save(PoliticalMail politicalMail) {



        super.save(politicalMail);
    }

    @Transactional(readOnly = false)
    public void delete(PoliticalMail politicalMail) {
        super.delete(politicalMail);
    }


    /**
     * 问政信息提交
     *
     * @param politicalMail
     * @return
     */
    @Transactional(readOnly = false)
    public int sendMail(PoliticalMail politicalMail) {
        //保存问政信息，如果非公开则生成查询码并发送邮件
        if ("2".endsWith(politicalMail.getOpen())) {
            politicalMail = doForNotOpenMail(politicalMail);
        }
        politicalMail.setState("0");// 信件状态（1未处理2已处理0处理中）
        politicalMail.setApplyTiime(new Date());// 信件提交时间
        super.save(politicalMail);
        //开启操作流程
        this.startMailFlowProcess(politicalMail);
        return 1;
    }


    /**
     * 该方法用于处理查询Mail的相关操作[生成查询码，生成查询密码以及邮件发送]
     *
     * @param politicalMail
     * @return
     */
    private PoliticalMail doForNotOpenMail(PoliticalMail politicalMail) {
        //查询码根据日期生成
        String queryCode = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        queryCode = sdf.format(new Date());
        politicalMail.setQueryCode(queryCode);
        politicalMail.setQueryPassword(PoliticalMailUtil.getRandomStr(6));//生成6位数密码
        //获取邮件配置信息并发送邮件
        EmailConfiger emailConfiger = new EmailConfiger();
        emailConfiger = emailConfigerService.findList(emailConfiger).get(0);
        PoliticalMailUtil.sendQueryMsgByEmail(politicalMail,emailConfiger);
        return politicalMail;
    }


    /**
     * 该方法用于开启流程
     *
     * @param entity
     */
    String handler = "thinkgem"; //流程的受理人暂时都用thinkgem
    private String  startMailFlowProcess(PoliticalMail entity) {
        // 设定流程参数[第一步启动所需要的参数]
        Map<String, Object> variables = new HashMap<String, Object>();
        if (StringUtils.isBlank(entity.getHdept())) {
            variables.put("msg", "1");// 如果受理部门为空，则任务流转至信息中心
            // 获取信息中心的处理信件任务的的专门用户[信息处理专员]，赋值给任务
            variables.put("handler", handler);
        } else {
            variables.put("msg", "2");// 如果受理部门不为空，则任务直接流转至该处理单位
            //为流程赋予下一步操作的角色
            variables.put("handler", String.valueOf(handler));
        }
        return actTaskService.startProcess(ActUtils.PD_POLITICAL_MAIL[0], ActUtils.PD_POLITICAL_MAIL[1], entity.getId(), entity.getSubject(),variables);
    }


    /**
     * 问政处理信息存储
     * @param politicalMail
     */
    @Transactional(readOnly = false)
    public void mailsave(PoliticalMail politicalMail,HttpServletRequest request) {
        Map<String, Object> vars = Maps.newHashMap();
        String msg = request.getParameter("msg");
        if(StringUtils.isNotBlank(msg)){
            vars.put("msg",msg);//决定是分发还是退回
        }
        vars.put("handler", handler);//任务所有人
        actTaskService.complete(politicalMail.getAct().getTaskId(), politicalMail.getAct().getProcInsId(), politicalMail.getAct().getComment(), vars);
    }

}
