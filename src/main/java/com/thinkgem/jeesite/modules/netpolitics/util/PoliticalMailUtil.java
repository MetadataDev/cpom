package com.thinkgem.jeesite.modules.netpolitics.util;

import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.emailconfig.entity.EmailConfiger;
import com.thinkgem.jeesite.modules.netpolitics.entity.PoliticalMail;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class PoliticalMailUtil {

    /**
     * 该方法用于计算预警状态
     */
    public static String getWarningState(PoliticalMail politicalMailEntity) {
        if ("1".equals(politicalMailEntity.getState())) {
            Date applyTiime = politicalMailEntity.getApplyTiime();
            // 计算今天和信件发送时间相差天数
            int daysBetween = 0;
            try {
                Date curruntDay = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                applyTiime = sdf.parse(sdf.format(applyTiime));
                curruntDay = sdf.parse(sdf.format(curruntDay));
                Calendar cal = Calendar.getInstance();
                cal.setTime(applyTiime);
                long time1 = cal.getTimeInMillis();
                cal.setTime(curruntDay);
                long time2 = cal.getTimeInMillis();
                long between_days = (time2 - time1) / (1000 * 3600 * 24);
                daysBetween = Integer.parseInt(String.valueOf(between_days));
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (daysBetween <= 1) {
                return "1";// 正常
            } else if (daysBetween > 1 && daysBetween <= 3) {
                return "2";// 警告
            } else {
                return "3";// 紧急
            }
        } else {
            return "1";// 正常
        }
    }

    /**
     * 该方法用于生成随机字符串
     *
     * @param num
     * @return
     */
    public static String getRandomStr(int num) {
        String randomStr = "";
        num = (num == 0) ? 1 : num;
        for (int i = 0; i < num; i++) {
            randomStr += String.valueOf((int) (Math.random() * 10));
        }
        return randomStr;
    }

    /**
     * 该方法用于发送email
     *
     * @param politicalMailEntity
     */
    public static void sendQueryMsgByEmail(PoliticalMail politicalMailEntity ,EmailConfiger emailConfiger) {
        // 从缓存中获取邮箱的相关配置
        String port = emailConfiger.getPort().trim();// 端口
        String server = emailConfiger.getServer().trim(); // 邮件服务器
        String from = emailConfiger.getFrom().trim(); // 发送者
        String fpassword = emailConfiger.getPassword().trim(); // 永不密码
        String contentType = emailConfiger.getContenttype();// 正文类型[做邮件内容使用]
        String messageText = "";// 邮件正文
        String toAddr = String.valueOf(politicalMailEntity.getMail()).trim();// 邮件发送地址
        if ("2".equals(politicalMailEntity.getState())) {
            // 如果信件已经处理，发送邮件告知处理结果
            String subject = "新洲公共服务平台-信件回复 ";// 邮件主题
            messageText = "来信主题：  " + politicalMailEntity.getSubject() + "          ";
            messageText += "信件查询码：  " + politicalMailEntity.getQueryCode() + "           ";
            messageText += "密码：  " + politicalMailEntity.getQueryPassword() + "           ";
//            messageText += "回复内容：  " + politicalMailEntity.getHandlerContent() + "           ";
            EmailUtil.sendMail(server, from, fpassword, toAddr, subject, messageText, port);
        } else {
            // 如果信件未回复，如果是非公开的，向指定邮箱发送邮件告知查询密码
            if ("2".equals(politicalMailEntity.getOpen())) {
                String subject = "新洲公共服务平台-信件查询码 ";// 邮件主题
                if (StringUtils.isBlank(contentType) || "null".equals(contentType)) {
                    messageText = "来信主题：" + politicalMailEntity.getSubject() + "          ";
                    messageText += "信件查询码：" + politicalMailEntity.getQueryCode() + "          ";
                    messageText += "密码：" + politicalMailEntity.getQueryPassword();
                } else {
                    // 替换预设字段
                    messageText = contentType;
                    messageText = messageText.replace("[mailSubject]", String.valueOf(politicalMailEntity.getSubject()));
                    messageText = messageText.replace("[queryCode]", String.valueOf(politicalMailEntity.getQueryCode()));
                    messageText = messageText.replace("[password]", String.valueOf(politicalMailEntity.getQueryPassword()));
                }
                EmailUtil.sendMail(server, from, fpassword, toAddr, subject, messageText, port);
            }

        }

    }

    /**
     * 时间格式转换
     */
    public static String dateFormat(Date date, String suffix) {
        if (date != null) {
            SimpleDateFormat sdf = new SimpleDateFormat(suffix);
            return sdf.format(date);
        } else {
            return "处理中…";
        }
    }

}
