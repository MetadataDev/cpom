/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.collectdata.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 采集数据管理Entity
 * @author dingshuang
 * @version 2016-07-10
 */
public class CollectData extends DataEntity<CollectData> {
	
	private static final long serialVersionUID = 1L;
	private String collectOrigin;		// 采集源
	private String originUrl;		// 源地址
	private String title;		// 标题
	private String content;		// 内容
	private String releaseTime;		// 发布时间
	private String viewCount;		// 浏览量
	private String transCount;		// 转发量
	private String replyCount;		// 回复量
	private String isIndex;		// 是否索引
	private String isTrans; //是否已经转办
	private String collectDate;		// 采集时间
	public CollectData() {
		super();
	}

	public CollectData(String id){
		super(id);
	}

	@Length(min=0, max=100, message="采集源长度必须介于 0 和 100 之间")
	public String getCollectOrigin() {
		return collectOrigin;
	}

	public void setCollectOrigin(String collectOrigin) {
		this.collectOrigin = collectOrigin;
	}
	
	@Length(min=0, max=200, message="源地址长度必须介于 0 和 200 之间")
	public String getOriginUrl() {
		return originUrl;
	}

	public void setOriginUrl(String originUrl) {
		this.originUrl = originUrl;
	}
	
	@Length(min=0, max=200, message="标题长度必须介于 0 和 200 之间")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public  String getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(String releaseTime) {
		this.releaseTime = releaseTime;
	}
	
	@Length(min=0, max=10, message="浏览量长度必须介于 0 和 10 之间")
	public String getViewCount() {
		return viewCount;
	}

	public void setViewCount(String viewCount) {
		this.viewCount = viewCount;
	}
	
	@Length(min=0, max=10, message="转发量长度必须介于 0 和 10 之间")
	public String getTransCount() {
		return transCount;
	}

	public void setTransCount(String transCount) {
		this.transCount = transCount;
	}
	
	@Length(min=0, max=10, message="回复量长度必须介于 0 和 10 之间")
	public String getReplyCount() {
		return replyCount;
	}

	public void setReplyCount(String replyCount) {
		this.replyCount = replyCount;
	}
	
	@Length(min=0, max=1, message="是否索引长度必须介于 0 和 1 之间")
	public String getIsIndex() {
		return isIndex;
	}

	public void setIsIndex(String isIndex) {
		this.isIndex = isIndex;
	}


	@Length(min=0, max=1, message="是否监控 0否1是长度必须介于 0 和 1 之间")
	public String getIsTrans() {
		return isTrans;
	}

	public void setIsTrans(String isTrans) {
		this.isTrans = isTrans;
	}

	public String getCollectDate() {
		return collectDate;
	}

	public void setCollectDate(String collectDate) {
		this.collectDate = collectDate;
	}
}