/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionWarning.service;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.opinionWarning.dao.OpinionWarningDao;
import com.thinkgem.jeesite.modules.opinionWarning.entity.OpinionWarning;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 舆情预警Service
 * @author dingshuang
 * @version 2016-07-12
 */
@Service
@Transactional(readOnly = true)
public class OpinionWarningService extends CrudService<OpinionWarningDao, OpinionWarning> {

	@Autowired
	public OpinionWarningDao opinionWarningDao;

	public OpinionWarning get(String id) {
		return super.get(id);
	}
	
	public List<OpinionWarning> findList(OpinionWarning opinionWarning) {
		return super.findList(opinionWarning);
	}
	
	public Page<OpinionWarning> findPage(Page<OpinionWarning> page, OpinionWarning opinionWarning) {
		return super.findPage(page, opinionWarning);
	}
	
	@Transactional(readOnly = false)
	public void save(OpinionWarning opinionWarning) {
		super.save(opinionWarning);
	}
	
	@Transactional(readOnly = false)
	public void delete(OpinionWarning opinionWarning) {
		super.delete(opinionWarning);
	}

	public int getOpinionWarningCount(String originUrl){
		return opinionWarningDao.getOpinionWarningCount(originUrl);
	}

	public int getCount(){
		return opinionWarningDao.getCount();
	}
	
}