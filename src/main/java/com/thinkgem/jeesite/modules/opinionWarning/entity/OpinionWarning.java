/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionWarning.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;


import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 舆情预警Entity
 * @author dingshuang
 * @version 2016-07-12
 */
public class OpinionWarning extends DataEntity<OpinionWarning> {
	
	private static final long serialVersionUID = 1L;
	private String collectOrigin;		// 采集源
	private String originUrl;		// 源地址
	private String title;		// 标题
	private String content;		// 内容
	private String releaseTime;		// 发布时间
	private String viewCount;		// 浏览量
	private String transCount;		// 转发量
	private String replyCount;		// 回复量
	private String isTrans;		// 是否已转办（0否1是）
	private String warningReason;		// 预警原因
	
	public OpinionWarning() {
		super();
	}

	public OpinionWarning(String id){
		super(id);
	}

	@Length(min=0, max=100, message="采集源长度必须介于 0 和 100 之间")
	public String getCollectOrigin() {
		return collectOrigin;
	}

	public void setCollectOrigin(String collectOrigin) {
		this.collectOrigin = collectOrigin;
	}
	
	@Length(min=0, max=200, message="源地址长度必须介于 0 和 200 之间")
	public String getOriginUrl() {
		return originUrl;
	}

	public void setOriginUrl(String originUrl) {
		this.originUrl = originUrl;
	}
	
	@Length(min=0, max=200, message="标题长度必须介于 0 和 200 之间")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(String releaseTime) {
		this.releaseTime = releaseTime;
	}
	
	@Length(min=0, max=10, message="浏览量长度必须介于 0 和 10 之间")
	public String getViewCount() {
		return viewCount;
	}

	public void setViewCount(String viewCount) {
		this.viewCount = viewCount;
	}
	
	@Length(min=0, max=10, message="转发量长度必须介于 0 和 10 之间")
	public String getTransCount() {
		return transCount;
	}

	public void setTransCount(String transCount) {
		this.transCount = transCount;
	}
	
	@Length(min=0, max=10, message="回复量长度必须介于 0 和 10 之间")
	public String getReplyCount() {
		return replyCount;
	}

	public void setReplyCount(String replyCount) {
		this.replyCount = replyCount;
	}
	
	@Length(min=0, max=1, message="是否已转办（0否1是）长度必须介于 0 和 1 之间")
	public String getIsTrans() {
		return isTrans;
	}

	public void setIsTrans(String isTrans) {
		this.isTrans = isTrans;
	}
	
	@Length(min=0, max=100, message="预警原因长度必须介于 0 和 100 之间")
	public String getWarningReason() {
		return warningReason;
	}

	public void setWarningReason(String warningReason) {
		this.warningReason = warningReason;
	}
	
}