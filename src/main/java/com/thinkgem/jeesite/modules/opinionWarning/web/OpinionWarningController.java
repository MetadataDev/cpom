/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionWarning.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.opinionmonitoring.entity.OpinionMonitoring;
import com.thinkgem.jeesite.modules.opinionmonitoring.service.OpinionMonitoringService;
import com.thinkgem.jeesite.modules.publicoption.entity.PublicOpinion;
import com.thinkgem.jeesite.modules.publicoption.service.PublicOpinionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.opinionWarning.entity.OpinionWarning;
import com.thinkgem.jeesite.modules.opinionWarning.service.OpinionWarningService;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * 舆情预警Controller
 * @author dingshuang
 * @version 2016-07-12
 */
@Controller
@RequestMapping(value = "${adminPath}/opinionwarning/opinionWarning")
public class OpinionWarningController extends BaseController {

	@Autowired
	private OpinionWarningService opinionWarningService;

	@Autowired
	private PublicOpinionService publicOpinionService;

	@Autowired
	private OpinionMonitoringService opinionMonitoringService;
	
	@ModelAttribute
	public OpinionWarning get(@RequestParam(required=false) String id) {
		OpinionWarning entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = opinionWarningService.get(id);
		}
		if (entity == null){
			entity = new OpinionWarning();
		}
		return entity;
	}
	
	@RequiresPermissions("opinionwarning:opinionWarning:view")
	@RequestMapping(value = {"list", ""})
	public String list(OpinionWarning opinionWarning, Page<OpinionWarning> page ,HttpServletRequest request, HttpServletResponse response, Model model) {
		page.setPageSize(5);
		page= opinionWarningService.findPage(page, opinionWarning);
		model.addAttribute("page", page);
		return "modules/opinionWarning/opinionWarningList";
	}

	@RequiresPermissions("opinionwarning:opinionWarning:view")
	@RequestMapping(value = "form")
	public String form(OpinionWarning opinionWarning, Model model) {
		model.addAttribute("opinionWarning", opinionWarning);
		return "modules/opinionWarning/opinionWarningForm";
	}

	@RequiresPermissions("opinionwarning:opinionWarning:edit")
	@RequestMapping(value = "save")
	public String save(OpinionWarning opinionWarning, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, opinionWarning)){
			return form(opinionWarning, model);
		}
		opinionWarningService.save(opinionWarning);
		addMessage(redirectAttributes, "保存舆情预警成功");
		return "redirect:"+Global.getAdminPath()+"/opinionarning/opinionWarning/?repage";
	}
	
	@RequiresPermissions("opinionwarning:opinionWarning:edit")
	@RequestMapping(value = "delete")
	public String delete(OpinionWarning opinionWarning, HttpServletRequest request,RedirectAttributes redirectAttributes) {
		opinionWarningService.delete(opinionWarning);
		//撤销舆情预警的同时，撤销监控
		addMessage(redirectAttributes, "撤销预警成功");
		opinionMonitoringService.delete(new OpinionMonitoring());
		return "redirect:"+Global.getAdminPath()+"/opinionwarning/opinionWarning/list?pageSize="+request.getParameter("pageSize")+"&pageNo="+request.getParameter("pageNo");
	}


	/**
	 * 批量删除监控舆情
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "toBatchDel")
	public void toBatchDel(HttpServletRequest request,HttpServletResponse response) throws IOException {
		String ids = request.getParameter("ids");
		PrintWriter out = response.getWriter();
		if(StringUtils.isNotBlank(ids)){
			String[] _ids = ids.split(";");
			if(ids!=null && ids.length()>0){
				for (String id : _ids){
					if(StringUtils.isNotBlank(id)){
						opinionWarningService.delete(new OpinionWarning(id));
					}
				}
			}
		}
		out.write(JSONObject.valueToString(1).toString());
	}


	/**
	 * 获取预警舆情总数
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "getCount")
	public void getCount(HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		int result  = opinionWarningService.getCount();
		out.write(JSONObject.valueToString(result).toString());
	}


	/**
	 * 转办
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "toTrans")
	public void toTrans(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String id = request.getParameter("id");
		PrintWriter out = response.getWriter();
		//删除预警信息
		opinionMonitoringService.delete(new OpinionMonitoring(id));
		//获取预警数据对象，并赋属性值到舆情中
		OpinionWarning opinionWarning = opinionWarningService.get(new OpinionWarning(id));
		opinionWarning.setIsTrans("1");//标记已经转办过了
		opinionWarningService.save(opinionWarning);
		//经预警处理发送至信息中心
		PublicOpinion publicOpinion = new PublicOpinion();
		BeanUtils.copyProperties(opinionWarning,publicOpinion);
		publicOpinion.setId(null);
		publicOpinion.setStatus("1");
		publicOpinion.setInvolveField(request.getParameter("involveField"));
		publicOpinion.setInvolveTownName(request.getParameter("involveTownName"));
		Object result  = publicOpinionService.publicOpinionInto(publicOpinion);
		out.write(JSONObject.valueToString(result).toString());
	}

}