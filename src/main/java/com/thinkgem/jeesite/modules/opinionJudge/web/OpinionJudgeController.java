/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionJudge.web;

import com.metadata.lucene.LuceneCollectDataSvc;
import com.metadata.warningCalculate.util.NumberUtil;
import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.collectdata.entity.CollectData;
import com.thinkgem.jeesite.modules.keywords.entity.KeyWords;
import com.thinkgem.jeesite.modules.keywords.service.KeyWordsService;
import com.thinkgem.jeesite.modules.opinionJudge.service.OpinionJudgeService;
import com.thinkgem.jeesite.modules.opinionmonitoring.entity.OpinionMonitoring;
import com.thinkgem.jeesite.modules.opinionmonitoring.service.OpinionMonitoringService;
import com.thinkgem.jeesite.modules.publicoption.entity.PublicOpinion;
import com.thinkgem.jeesite.modules.publicoption.service.PublicOpinionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * 舆情研判Controller
 *
 * @author dingshuang
 * @version 2016-06-21
 */
@Controller
@RequestMapping(value = "${adminPath}/opinionJudge/opinionJudge")
public class OpinionJudgeController extends BaseController {

    @Autowired
    private LuceneCollectDataSvc luceneCollectDataSvc;

    @Autowired
    private KeyWordsService keyWordsService;

    @Autowired
    private PublicOpinionService publicOpinionService;

    @Autowired
    private OpinionJudgeService opinionJudgeService;

    @Autowired
    private OpinionMonitoringService opinionMonitoringService;

    /**
     * 跳转舆情预警页面
     *
     * @param page
     * @param model
     * @return
     */
    @RequiresPermissions("opinionJudge:opinionJudge:view")
    @RequestMapping(value = {"list", ""})
    public String list(Page page,HttpServletRequest request, Model model) {
        String keyWorld = request.getParameter("keyWorld");
        if(StringUtils.isBlank(keyWorld)){
            //关键词输入为空，则按照预定关键词列表查询
            List<KeyWords> queryStrings = keyWordsService.getUseableWords();
            if (queryStrings != null && queryStrings.size() > 0) {
                Page<CollectData> _page = luceneCollectDataSvc.getResultPage(page, queryStrings);
                model.addAttribute("page", _page);
            } else {
                model.addAttribute("page", new Page<CollectData>());
            }
        }else{
            List<KeyWords> queryStrings = new ArrayList<KeyWords>();
            KeyWords keyWords = new KeyWords();
            keyWords.setWord(keyWorld);
            queryStrings.add(keyWords);
            Page<CollectData> _page = luceneCollectDataSvc.getResultPage(page, queryStrings);
            model.addAttribute("page", _page);
            model.addAttribute("keyWorld", keyWorld);
        }
        return "modules/opinionJudge/opinionJudgeList";
    }


    /**
     * 删除研判的数据
     */
    @RequestMapping(value = "delete")
    public String delete(HttpServletRequest request, RedirectAttributes redirectAttributes) {
        String id = request.getParameter("id");
        //删除索引以及数据
        luceneCollectDataSvc.deleteIndex(id);
        opinionJudgeService.deleteCollectData(new CollectData(id));
        addMessage(redirectAttributes, "删除研判信息成功");
        return "redirect:"+ Global.getAdminPath()+"/opinionJudge/opinionJudge/list?pageSize="+request.getParameter("pageSize")+"&pageNo="+request.getParameter("pageNo");
    }

    /**
     * 批量删除研判信息
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "toBatchDel")
    public void toBatchDel(HttpServletRequest request,HttpServletResponse response) throws IOException {
        String ids = request.getParameter("ids");
        PrintWriter out = response.getWriter();
        if(StringUtils.isNotBlank(ids)){
            String[] _ids = ids.split(";");
            if(ids!=null && ids.length()>0){
                for (String id : _ids){
                    if(StringUtils.isNotBlank(id)){
                        luceneCollectDataSvc.deleteIndex(id);
                        opinionJudgeService.deleteCollectData(new CollectData(id));
                    }
                }
            }
        }
        out.write(JSONObject.valueToString(1).toString());
    }


    /**
     * 添加到监控
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "toMonitoring")
    public void toMonitoring(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        PrintWriter out = response.getWriter();
        //删除索引以及数据
        luceneCollectDataSvc.deleteIndex(id);
        opinionJudgeService.deleteCollectData(new CollectData(id));
        //生成监控对象并保存
        CollectData collectData = opinionJudgeService.getCollectData(new CollectData(id));
        OpinionMonitoring opinionMonitoring = new OpinionMonitoring();
        BeanUtils.copyProperties(collectData,opinionMonitoring);
        //该方法用于处理访问量、转发量、回复量的异常
        opinionMonitoring.setViewCount(NumberUtil.updateExceptionNum(opinionMonitoring.getViewCount()));
        opinionMonitoring.setTransCount(NumberUtil.updateExceptionNum(opinionMonitoring.getTransCount()));
        opinionMonitoring.setReplyCount(NumberUtil.updateExceptionNum(opinionMonitoring.getReplyCount()));
        opinionMonitoring.setId(null);
        opinionMonitoring.setDelFlag("0");
        opinionMonitoring.setStatus(request.getParameter("monitoringField"));
        opinionMonitoringService.save(opinionMonitoring);
        out.write(JSONObject.valueToString(1).toString());
    }



    /**
     * 转办（不跨域）
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "toTrans")
    public void toTrans(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        PrintWriter out = response.getWriter();
        //删除索引以及数据，避免再次被命中研判
        luceneCollectDataSvc.deleteIndex(id);
        //CollectData 数据采集管理实类
        opinionJudgeService.deleteCollectData(new CollectData(id));
        //获取采集数据对象，并赋属性值到舆情中
        CollectData collectData = opinionJudgeService.getCollectData(new CollectData(id));
        collectData.setIsTrans("1");
        opinionJudgeService.updateCollectData(collectData);
        //PublicOpinion 舆论实类
        PublicOpinion publicOpinion = new PublicOpinion();
        BeanUtils.copyProperties(collectData,publicOpinion);
        publicOpinion.setId(null);
        publicOpinion.setStatus("1");
        publicOpinion.setInvolveField(request.getParameter("involveField"));
        publicOpinion.setInvolveTownName(request.getParameter("involveTownName"));
        Object result  = publicOpinionService.publicOpinionInto(publicOpinion);
        out.write(JSONObject.valueToString(result).toString());
    }


    /**
     * 获取需要研判的数据的总量
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "getCount")
    public void getCount(HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        String  result = "0"; //需要研判的数据总量
        List<KeyWords> queryStrings = keyWordsService.getUseableWords();
        if (queryStrings != null && queryStrings.size() > 0) {
            int count = luceneCollectDataSvc.getResultCount(queryStrings);
            result = (count >=100)?"99+":String.valueOf(count);
        }
        out.write(JSONObject.valueToString(result).toString());
    }

}