/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.warningCondition.service;

import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.warningCondition.dao.WarningConditionDao;
import com.thinkgem.jeesite.modules.warningCondition.entity.WarningCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 监控信息管理Service
 * @author dingshuang
 * @version 2016-07-11
 */
@Service
@Transactional(readOnly = true)
public class WarningConditionService extends CrudService<WarningConditionDao, WarningCondition> {

    /**
     * 保存预警条件
     * @param warningParams
     * @return
     */
	@Transactional(readOnly = false)
	public int  saveWarningParams(String warningParams){
		int result = 0;
		if(StringUtils.isNotBlank(warningParams)){
			String [] wps = warningParams.split("-");
			if(wps!=null&&wps.length>0){
				for (String wp:wps) {
					if(StringUtils.isNotBlank(wp)){
						String strs[] = wp.split("_");
                        WarningCondition warningCondition = new WarningCondition();
                        warningCondition.setId(strs[0]);
                        warningCondition.setFunc(strs[1]);
                        warningCondition.setIsUsed(strs[2]);
                        warningCondition.setParam(strs[3]);
						super.save(warningCondition);
						result++;
					}
				}
			}
		}
		return result>0?1:0;
	}
}