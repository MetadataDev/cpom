/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.emailconfig.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.emailconfig.entity.EmailConfiger;
import com.thinkgem.jeesite.modules.emailconfig.dao.EmailConfigerDao;

/**
 * 邮箱信息配置Service
 * @author dingshuang
 * @version 2016-06-15
 */
@Service
@Transactional(readOnly = true)
public class EmailConfigerService extends CrudService<EmailConfigerDao, EmailConfiger> {

	public EmailConfiger get(String id) {
		return super.get(id);
	}
	
	public List<EmailConfiger> findList(EmailConfiger emailConfiger) {
		return super.findList(emailConfiger);
	}
	
	public Page<EmailConfiger> findPage(Page<EmailConfiger> page, EmailConfiger emailConfiger) {
		return super.findPage(page, emailConfiger);
	}
	
	@Transactional(readOnly = false)
	public void save(EmailConfiger emailConfiger) {
		super.save(emailConfiger);
	}
	
	@Transactional(readOnly = false)
	public void delete(EmailConfiger emailConfiger) {
		super.delete(emailConfiger);
	}
	
}