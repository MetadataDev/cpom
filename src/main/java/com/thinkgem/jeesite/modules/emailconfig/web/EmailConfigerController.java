/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.emailconfig.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.emailconfig.entity.EmailConfiger;
import com.thinkgem.jeesite.modules.emailconfig.service.EmailConfigerService;

/**
 * 邮箱信息配置Controller
 * @author dingshuang
 * @version 2016-06-15
 */
@Controller
@RequestMapping(value = "${adminPath}/emailconfig/emailConfiger")
public class EmailConfigerController extends BaseController {

	@Autowired
	private EmailConfigerService emailConfigerService;
	
	@ModelAttribute
	public EmailConfiger get(@RequestParam(required=false) String id) {
		EmailConfiger entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = emailConfigerService.get(id);
		}
		if (entity == null){
			entity = new EmailConfiger();
		}
		return entity;
	}
	
	@RequiresPermissions("emailconfig:emailConfiger:view")
	@RequestMapping(value = {"list", ""})
	public String list(EmailConfiger emailConfiger, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<EmailConfiger> page = emailConfigerService.findPage(new Page<EmailConfiger>(request, response), emailConfiger); 
		model.addAttribute("page", page);
		return "modules/emailconfig/emailConfigerList";
	}

	@RequiresPermissions("emailconfig:emailConfiger:view")
	@RequestMapping(value = "form")
	public String form(EmailConfiger emailConfiger, Model model) {
		emailConfiger = emailConfigerService.findList(emailConfiger).get(0);
		model.addAttribute("emailConfiger", emailConfiger);
		return "modules/emailconfig/emailConfigerForm";
	}

	@RequiresPermissions("emailconfig:emailConfiger:edit")
	@RequestMapping(value = "save")
	public String save(EmailConfiger emailConfiger, Model model, RedirectAttributes redirectAttributes) {
		emailConfigerService.save(emailConfiger);
		addMessage(redirectAttributes, "保存邮箱信息配置成功");
		return "redirect:form";
	}
	
	@RequiresPermissions("emailconfig:emailConfiger:edit")
	@RequestMapping(value = "delete")
	public String delete(EmailConfiger emailConfiger, RedirectAttributes redirectAttributes) {
		emailConfigerService.delete(emailConfiger);
		addMessage(redirectAttributes, "删除邮箱信息配置成功");
		return "redirect:"+Global.getAdminPath()+"/emailconfig/emailConfiger/?repage";
	}

}