/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.emailconfig.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 邮箱信息配置Entity
 * @author dingshuang
 * @version 2016-06-15
 */
public class EmailConfiger extends DataEntity<EmailConfiger> {
	
	private static final long serialVersionUID = 1L;
	private String Server;		// 服务器
	private String Port;		// 端口
	private String From;		// 发送邮箱
	private String Password;		// 邮箱密码
	private String To;		// 目的邮箱
	private String contenttype;		// 内容类型[做邮件内容使用]
	
	public EmailConfiger() {
		super();
	}

	public EmailConfiger(String id){
		super(id);
	}

	@Length(min=1, max=20, message="服务器长度必须介于 1 和 20 之间")
	public String getServer() {
		return Server;
	}

	public void setServer(String Server) {
		this.Server = Server;
	}
	
	@Length(min=1, max=10, message="端口长度必须介于 1 和 10 之间")
	public String getPort() {
		return Port;
	}

	public void setPort(String Port) {
		this.Port = Port;
	}
	
	@Length(min=1, max=20, message="发送邮箱长度必须介于 1 和 20 之间")
	public String getFrom() {
		return From;
	}

	public void setFrom(String From) {
		this.From = From;
	}
	
	@Length(min=1, max=20, message="邮箱密码长度必须介于 1 和 20 之间")
	public String getPassword() {
		return Password;
	}

	public void setPassword(String Password) {
		this.Password = Password;
	}
	
	@Length(min=1, max=20, message="目的邮箱长度必须介于 1 和 20 之间")
	public String getTo() {
		return To;
	}

	public void setTo(String To) {
		this.To = To;
	}
	
	@Length(min=0, max=1000, message="内容类型[做邮件内容使用]长度必须介于 0 和 1000 之间")
	public String getContenttype() {
		return contenttype;
	}

	public void setContenttype(String contenttype) {
		this.contenttype = contenttype;
	}
	
}