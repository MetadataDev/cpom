/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.sys.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.service.TreeService;
import com.thinkgem.jeesite.modules.sys.dao.OfficeDao;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * 机构Service
 * @author ThinkGem
 * @version 2014-05-16
 */
@Service
@Transactional(readOnly = true)
public class OfficeService extends TreeService<OfficeDao, Office> {

	public List<Office> findAll(){
		return UserUtils.getOfficeList();
	}

	public List<Office> findList(Boolean isAll){
		if (isAll != null && isAll){
			return UserUtils.getOfficeAllList();
		}else{
			return UserUtils.getOfficeList();
		}
	}
	
	@Transactional(readOnly = true)
	public List<Office> findList(Office office){
		if(office != null){
			office.setParentIds(office.getParentIds()+"%");
			return dao.findByParentIdsLike(office);
		}
		return  new ArrayList<Office>();
	}
	
	@Transactional(readOnly = false)
	public void save(Office office) {
		super.save(office);
		UserUtils.removeCache(UserUtils.CACHE_OFFICE_LIST);
	}
	
	@Transactional(readOnly = false)
	public void delete(Office office) {
		super.delete(office);
		UserUtils.removeCache(UserUtils.CACHE_OFFICE_LIST);
	}


    /**
	 * 该方法用于获取所有的叶子级别机构
	 * @param office
	 * @param leafOffices
	 * @return
     */
	public List<Map<String,Object>> getAllLeafOffice(Office office,List<Map<String,Object>> leafOffices){
		office.setParentIds(office.getParentIds()+office.getId()+"%");
		List<Office> list = dao.findByParentIdsLike(office);
		if(list!=null&&list.size()>0){
			//还有子节点，则一一回调
			for (Office offi:list) {
				getAllLeafOffice(offi,leafOffices);
			}
		}else{
        	//证明是叶子节点
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("departname",office.getName());
			map.put("departId",office.getId());
			leafOffices.add(map);
		}
        return leafOffices;
	}
	
}
