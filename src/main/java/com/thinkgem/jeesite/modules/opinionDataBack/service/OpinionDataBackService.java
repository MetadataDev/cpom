/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionDataBack.service;

import com.metadata.lucene.LuceneCollectData;
import com.metadata.lucene.LuceneCollectDataSvc;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.collectdata.dao.CollectDataDao;
import com.thinkgem.jeesite.modules.collectdata.entity.CollectData;
import com.thinkgem.jeesite.modules.opinionWarning.dao.OpinionWarningDao;
import com.thinkgem.jeesite.modules.opinionWarning.entity.OpinionWarning;
import com.thinkgem.jeesite.modules.opinionmonitoring.dao.OpinionMonitoringDao;
import com.thinkgem.jeesite.modules.opinionmonitoring.entity.OpinionMonitoring;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;

/**
 * 舆情研判Service
 * @author dingshuang
 * @version 2016-06-21
 */
@Service
@Transactional(readOnly = true)
public class OpinionDataBackService extends CrudService<CollectDataDao, CollectData> {

	@Autowired
	private CollectDataDao  collectDataDao;

	@Autowired
	private OpinionMonitoringDao opinionMonitoringDao;

	@Autowired
	private OpinionWarningDao opinionWarningDao;

	/**
	 * 获取被删除研判数据
	 * @param page
	 * @param entity
     * @return
     */
	@Transactional(readOnly = false)
	public Page<CollectData> getDeleteCollectDataPage(Page<CollectData> page,CollectData entity){
		entity.setPage(page);
		page.setList(collectDataDao.findDeleteCollectDataList(entity));
		return page;
	}

	/**
	 * 获取删除的监控数据
	 * @param page
	 * @param entity
	 * @return
	 */
	@Transactional(readOnly = false)
	public Page<OpinionMonitoring> getDeleteMonitoringOpinionPage(Page<OpinionMonitoring> page, OpinionMonitoring entity){
		entity.setPage(page);
		page.setList(opinionMonitoringDao.findDeleteMonitoringOpinionList(entity));
		return page;
	}


	/**
	 * 获取删除的预警数据
	 * @param page
	 * @param entity
     * @return
     */
	@Transactional(readOnly = false)
	public Page<OpinionWarning> getDeleteOpinionWarningPage(Page<OpinionWarning> page, OpinionWarning entity){
		entity.setPage(page);
		page.setList(opinionWarningDao.getDeleteOpinionWarningPage(entity));
		return page;
	}

	/**
	 * 恢复研判数据
	 * @param id
     */
	@Transactional(readOnly = false)
	public void judgeDataBack (String id) throws IOException{
		collectDataDao.reverseDelete(new CollectData(id));
		CollectData collectData = collectDataDao.get(new CollectData(id));
        Directory dir = new SimpleFSDirectory(new File(LuceneCollectDataSvc.LUCENE_PATH));
        boolean exist = IndexReader.indexExists(dir);
        IndexWriter writer = new IndexWriter(dir, new StandardAnalyzer(
                Version.LUCENE_30), !exist, IndexWriter.MaxFieldLength.LIMITED);
		try {
			writer.addDocument(LuceneCollectData.createDocument(collectData));
			collectData.setIsIndex("1");//标记为已创建索引
			collectDataDao.update(collectData);
			writer.optimize();
		}finally {
			writer.close();
		}
	}


	/**
	 * 恢复监控数据
	 * @param id
	 */
	@Transactional(readOnly = false)
	public void monitoringDataBack (String id) {
		opinionMonitoringDao.reverseDelete(new OpinionMonitoring(id));
	}

	/**
	 * 恢复监控数据
	 * @param id
	 */
	@Transactional(readOnly = false)
	public void warningDataBack (String id) {
		opinionWarningDao.reverseDelete(new OpinionWarning(id));
	}


	@Transactional(readOnly = false)
	public Page<OpinionMonitoring> getOpinionMonitoringPage(Page<OpinionMonitoring> page, OpinionMonitoring entity){
		entity.setPage(page);
		page.setList(opinionMonitoringDao.findOpinionMonitoringRecovery(entity));
		return page;
	}

	@Transactional(readOnly = false)
	public Page<OpinionMonitoring> getGeneralMonitoringPage(Page<OpinionMonitoring> page, OpinionMonitoring entity){
		entity.setPage(page);
		page.setList(opinionMonitoringDao.findGeneralMonitoringRecovery(entity));
		return page;
	}
}