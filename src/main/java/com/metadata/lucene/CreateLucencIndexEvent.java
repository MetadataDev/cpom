package com.metadata.lucene;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by dingshuang
 */
public class CreateLucencIndexEvent implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private  LuceneCollectDataSvc luceneCollectDataSvc;


    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if(event.getApplicationContext().getParent() == null){//root application context 没有parent，他就是老大.
            //需要执行的逻辑代码，当spring容器初始化完成后就会执行该方法。
            luceneCollectDataSvc.createIndexLoop();
        }
    }


    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        context.close();
    }

}
