package com.metadata.lucene;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.CacheUtils;
import com.thinkgem.jeesite.modules.collectdata.dao.CollectDataDao;
import com.thinkgem.jeesite.modules.collectdata.entity.CollectData;
import com.thinkgem.jeesite.modules.keywords.entity.KeyWords;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class LuceneCollectDataSvc {

    private static Logger logger = LoggerFactory.getLogger(LuceneCollectDataSvc.class);

    //设定lucene的索引创建存放路径
    public static final String LUCENE_PATH = "d:/lucene/index";

    /**
     * 该方法用于循环创建索引
     */
    public void createIndexLoop() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Object isIndexOver = CacheUtils.get("LUCENE_CACHE", "isIndexOver");
                        if (isIndexOver == null || (Boolean.valueOf(isIndexOver.toString()))) {
                            createIndex(getCollectDataNotIndex());
                            logger.info("--------->已经完成对采集数据的研判分析计算……");
                        }
                        Thread.sleep(1000 * 60 * 2);//每隔一段时间创建一次索引
                        //TODO 后期应该改写当前程序，使用quartz进行定时任务调度
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }


    /**
     * 该方法用于创建索引数据
     *
     * @param collectDatas
     * @throws IOException
     * @throws ParseException
     * @throws java.text.ParseException
     */
    @Transactional(readOnly = true)
    public void createIndex(List<CollectData> collectDatas)
            throws IOException, ParseException, java.text.ParseException {
        CacheUtils.put("LUCENE_CACHE", "isIndexOver", false);
        if (collectDatas != null && collectDatas.size() > 0) {
            Directory dir = new SimpleFSDirectory(new File(LUCENE_PATH));
            boolean exist = IndexReader.indexExists(dir);
            IndexWriter writer = new IndexWriter(dir, new StandardAnalyzer(
                    Version.LUCENE_30), !exist, IndexWriter.MaxFieldLength.LIMITED);
            try {
                for (CollectData collectData : collectDatas) {
                    //判断当前url所对应的一串数据是否已经创建过索引
                    int count = collectDataDao.getGroupIsIndexCount(collectData.getOriginUrl());
                    if (!(count > 0)) {
                        writer.addDocument(LuceneCollectData.createDocument(collectData));
                        collectData.setIsIndex("1");//标记为已创建索引
                        collectDataDao.update(collectData);
                    }
                }
                writer.optimize();
            } finally {
                writer.close();
                CacheUtils.put("LUCENE_CACHE", "isIndexOver", true);
            }
        }
    }


    /**
     * 获取全文检索分页结果
     *
     * @param page
     * @return
     */
    public Page<CollectData> getResultPage(Page<CollectData> page, List<KeyWords> queryStrings) {
        Directory dir = null;
        try {
            dir = new SimpleFSDirectory(new File(LUCENE_PATH));
            Searcher searcher = new IndexSearcher(dir);
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);
            Query query = LuceneCollectData.createQuery(queryStrings, null, null, analyzer);
            int pageNo = page.getPageNo();
            int pageSize = page.getPageSize() > 0 ? page.getPageSize() : 5;
            Sort sort = new Sort(new SortField("releaseTime", SortField.STRING_VAL));
            int params[] = getQueryLenAndStar(pageNo, pageSize, searcher.search(query, 1).totalHits);
            TopDocs docs = searcher.search(query, null, params[0], sort); //获取的总量为  params[0]
            List<String> ids = LuceneCollectData.getResultPage(searcher, docs, params);
            //通过id获取 CollectData
            List<CollectData> collectDatas = new ArrayList<CollectData>();
            if (ids != null && ids.size() > 0) {
                for (String id : ids) {
                    CollectData collectData = collectDataDao.get(new CollectData(id));
                    collectDatas.add(collectData);
                }
            }
            page.setPageNo(pageNo);
            page.setPageSize(pageSize);
            page.setCount(docs.totalHits);
            page.setList(collectDatas);
            searcher.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return page;
    }


    /**
     * 该方法用于获取数据总量
     *
     * @param queryStrings
     * @return
     */
    public int getResultCount(List<KeyWords> queryStrings) {
        try {
            Directory dir = new SimpleFSDirectory(new File(LUCENE_PATH));
            Searcher searcher = new IndexSearcher(dir);
            Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_30);
            Query query = LuceneCollectData.createQuery(queryStrings, null, null, analyzer);
            TopDocs docs = searcher.search(query, 1);
            return docs.totalHits;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }


    /**
     * 该方法用于根据id删除索引
     *
     * @param id
     */
    public void deleteIndex(String id) {
        try {
            Directory dir = new SimpleFSDirectory(new File(LUCENE_PATH));
            boolean exist = IndexReader.indexExists(dir);
            IndexWriter writer = new IndexWriter(dir, new StandardAnalyzer(
                    Version.LUCENE_30), !exist, IndexWriter.MaxFieldLength.LIMITED);
            LuceneCollectData.delete(id, writer);
            writer.optimize();
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 计算此次需要的查询长度 和查询起点
     *
     * @param pageNo
     * @param pageSize
     * @param total
     * @return
     */
    private int[] getQueryLenAndStar(int pageNo, int pageSize, int total) {
        int[] params = new int[2]; //第一个为“lucene的查询总量（结束点）”，第二个为“查询的起点”
        params[1] = total - pageNo * pageSize;
        params[1] = (params[1] >= 0) ? params[1] : 0;
        params[0] = total - (pageNo - 1) * pageSize;
        params[0] = (params[0] > 0) ? params[0] : total + params[0];
        return params;
    }


    /**
     * 该方法用于获取未创建索引的采集数据
     *
     * @return
     */
    private List<CollectData> getCollectDataNotIndex() {
        return collectDataDao.getCollectDataNotIndex();
    }


    @Autowired
    private CollectDataDao collectDataDao;

    @Autowired
    public void setCollectDataDao(CollectDataDao collectDataDao) {
        this.collectDataDao = collectDataDao;
    }

}
