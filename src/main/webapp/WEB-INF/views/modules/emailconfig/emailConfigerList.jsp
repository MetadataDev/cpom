<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>邮箱信息配置管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/emailconfig/emailConfiger/">邮箱信息配置列表</a></li>
		<shiro:hasPermission name="emailconfig:emailConfiger:edit"><li><a href="${ctx}/emailconfig/emailConfiger/form">邮箱信息配置添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="emailConfiger" action="${ctx}/emailconfig/emailConfiger/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<shiro:hasPermission name="emailconfig:emailConfiger:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="emailConfiger">
			<tr>
				<shiro:hasPermission name="emailconfig:emailConfiger:edit"><td>
    				<a href="${ctx}/emailconfig/emailConfiger/form?id=${emailConfiger.id}">修改</a>
					<a href="${ctx}/emailconfig/emailConfiger/delete?id=${emailConfiger.id}" onclick="return confirmx('确认要删除该邮箱信息配置吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>