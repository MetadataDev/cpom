<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<>
<head>
	<title>网络问政管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			$("input[name='btnSubmit']").bind("click",function(){
				if($(this).val()=='办理'){
					$("input[name='msg']").val(1);
				}else if($(this).val()=='退回'){
					$("input[name='msg']").val(2);
				}else if($(this).val()=='分发'){
					$("input[name='msg']").val(1);
				}
			});
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/netpolitics/politicalMail/">网络问政列表</a></li>
		<li class="active"><a href="${ctx}/netpolitics/politicalMail/form?id=${politicalMail.id}">网络问政<shiro:hasPermission name="netpolitics:politicalMail:edit">${not empty politicalMail.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="netpolitics:politicalMail:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="politicalMail" action="${ctx}/netpolitics/politicalMail/mailsave" method="post" class="form-horizontal">
		<input name="msg" id="msg" type="hidden"/>
		<form:hidden path="id"/>
		<form:hidden path="processInstanceId"/>
		<form:hidden path="act.taskId"/>
		<form:hidden path="queryCode"/>
		<form:hidden path="queryPassword"/>
		<form:hidden path="mailType"/>
		<sys:message content="${message}"/>
		<div class="control-group">
			<label class="control-label">来信主题：</label>
			<div class="controls">
				<form:input path="subject" htmlEscape="false" maxlength="500" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">姓名：</label>
			<div class="controls">
				<form:input path="name" htmlEscape="false" maxlength="100" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">联系电话：</label>
			<div class="controls">
				<form:input path="mobile" htmlEscape="false" maxlength="15" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">来访人数：</label>
			<div class="controls">
				<form:input path="pcount" htmlEscape="false" maxlength="10" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">家庭住址：</label>
			<div class="controls">
				<form:input path="addr" htmlEscape="false" maxlength="100" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">电子邮件：</label>
			<div class="controls">
				<form:input path="mail" htmlEscape="false" maxlength="50" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">来访目的：</label>
			<div class="controls">
				<form:select path="aproval" class="input-xlarge required">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mail_aproval')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">问题分类：</label>
			<div class="controls">
				<form:select path="classify" class="input-xlarge required">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mail_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">信件内容：</label>
			<div class="controls">
				<form:input path="mcontent" htmlEscape="false" class="input-xlarge required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">是否公开：</label>
			<div class="controls">
				<form:radiobuttons path="open" items="${fns:getDictList('mail_open')}" itemLabel="label" itemValue="value" htmlEscape="false" class="required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">处理状态：</label>
			<div class="controls">
				<form:select path="state" class="input-xlarge ">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mail_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">提交时间：</label>
			<div class="controls">
				<input name="applyTiime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
					value="<fmt:formatDate value="${politicalMail.applyTiime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">附件：</label>
			<div class="controls">
				<a>暂无附件</a>
				<form:hidden path="attach" htmlEscape="false" maxlength="100" class="input-xlarge "/>
			</div>
		</div>
		<c:if test="${task eq 'usertask2'}">
			<div class="control-group">
				<label class="control-label">受理部门：</label>
				<div class="controls">
					<sys:treeselect id="hdept" name="office.id" value="${user.office.id}" labelName="office.name" labelValue="${user.office.name}"
									 title="部门" url="/sys/office/treeData?type=2" cssClass="input-xlarge" allowClear="true" notAllowSelectParent="true"/>
				</div>
			</div>
		</c:if>
		<c:if test="${task eq 'usertask1'}">
			<div class="control-group">
				<label class="control-label">办理时间：</label>
				<div class="controls">
					<input name="handleTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate "
						value="<fmt:formatDate value="${politicalMail.handleTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
						onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">受理人：</label>
				<div class="controls">
					<form:input path="handler" htmlEscape="false" maxlength="36" class="input-xlarge "/>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">回复内容：</label>
				<div class="controls">
					<form:input path="handlerContent" htmlEscape="false" class="input-xlarge "/>
				</div>
			</div>
		</c:if>
		<div class="form-actions">
			<shiro:hasPermission name="netpolitics:politicalMail:edit">
				<c:if test="${task eq 'usertask1'}">
					<input name="btnSubmit" class="btn btn-primary" type="submit" value="办理"/>&nbsp;
					<input name="btnSubmit" class="btn btn-primary" type="submit" value="退回"/>&nbsp;
				</c:if>
				<c:if test="${task eq 'usertask2'}">
					<input name="btnSubmit" class="btn btn-primary" type="submit" value="分发"/>&nbsp;
				</c:if>
			</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
		<c:if test="${not empty politicalMail.id}">
			<act:histoicFlow procInsId="${politicalMail.act.procInsId}" />
		</c:if>
	</form:form>
</body>
</html>