<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>关键词管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//$("#name").focus();
			$("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/keywords/keyWords/">关键词管理列表</a></li>
		<li class="active"><a href="${ctx}/keywords/keyWords/form?id=${keyWords.id}">关键词管理<shiro:hasPermission name="keywords:keyWords:edit">${not empty keyWords.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="keywords:keyWords:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="keyWords" action="${ctx}/keywords/keyWords/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>		
		<div class="control-group">
			<label class="control-label">关键词：</label>
			<div class="controls">
				<form:input path="word" htmlEscape="false" maxlength="200" class="input-xlarge "/>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">是否可用：</label>
			<div class="controls">
				<input name="usable" id="usable1" value="1" type="radio" <c:if test="${keyWords.usable != '0'}">checked</c:if>/>是&nbsp;&nbsp;
				<input name="usable" id="usable2" value="0" type="radio" <c:if test="${keyWords.usable eq '0'}">checked</c:if>/>否
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="keywords:keyWords:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>